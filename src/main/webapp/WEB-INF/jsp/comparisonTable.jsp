<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8"  src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8"  src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
<%--
  Created by IntelliJ IDEA.
  User: skyb
  Date: 09.10.19
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Таблица сравнения</title>
</head>
<body>


<table >
    <tr>
        <td>
            <table id="firstTable" border="1">

                <thead>
                <tr><td>Фирма</td><td>Номер</td><td>Название</td><td>Цена</td></tr>
                </thead>
                <tbody>

                <c:forEach items="${data.firstTableList}" var="entry" varStatus="tagStatus">
                    <tr>
                        <td nowrap>${entry.firm}</td>
                        <td>${entry.number}</td>
                        <td nowrap>${entry.name}</td>
                        <td><div contenteditable>${entry.price}</div></td>


                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </td>
        <td>
            <table id="secondTable" border="1"  margin-top="50%">
                <thead>
                <tr>
                    <th>Фирма1</th><th>Цена1</th><th>Наличие1</th><th>Срок1</th>
                    <th>Фирма2</th><th>Цена2</th><th>Наличие2</th><th>Срок2</th>
                    <th>Фирма3</th><th>Цена3</th><th>Наличие3</th><th>Срок3</th>
                </tr>
                <thead>
                <tbody>
                <c:forEach items="${data.searchBeanList}" var="entry1">
                    <tr>
                        <c:forEach items="${entry1}" var="entry2">
                            <td>${entry2.firma}</td>
                            <td bgcolor=${entry2.color}>${entry2.price}</td>
                            <td>${entry2.numberOfAvailable}</td>
                            <td>${entry2.numberOfDaysSupply}</td>
                        </c:forEach>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </td>
    </tr>

</table>
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#firstTable').DataTable( {
            "searching": false,
            "paging": false,
            "info": false,
            "scrollX": false,
            "scrollY": false,
            dom: 'Bfrtip',

            buttons: [
                'excel', 'pdf', 'print'
            ],
            "sorting": false
        } );
        $('#firstTable tbody').on( 'focusout', 'td', function () {
            table.cell( this ).invalidate().draw();
        } );
    } );
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#secondTable').DataTable({
            "searching": false,
            "paging": false,
            "info": false,
            "scrollX": false,
            "scrollY": false,
            "sorting": false,
            dom: 'Bfrtip',

            buttons: [
                'excel'
            ],
        });
    });
</script>

${error}




</body>
</html>
