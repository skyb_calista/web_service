
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title> Загрузка файла </title>

</head>

<body>

<c:url value="/uploadExcelFile" var="uploadFileUrl" />
<form method="post" enctype="multipart/form-data"
      action="${uploadFileUrl}">
    <input type="file" name="file" accept=".xls,.xlsx" /> <input
        type="submit" value="Загрузить" />
</form>
<form name="test" method="post" action="setAuthData">
    Текущие авторизационные данные


    <c:if test="${empty login}">
        <p>Логин 411960 </p>
    </c:if>
    <c:if test="${not empty login}">
        <p>Логин ${login} </p>
    </c:if>

    <c:if test="${empty auth}">
        <p>Авторизация: успешно </p>
    </c:if>
    <c:if test="${not empty auth}">
        <p>Авторизация: ${auth} </p>
    </c:if>

    <p>Логин<br>
        <input name = "login">
    </p>
    <p>Пароль<br>
        <input name = "password">
    </p>
    <p><input type="submit" value="Сохранить">
</form>
</body>

</html>