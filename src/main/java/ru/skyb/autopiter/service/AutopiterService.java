package ru.skyb.autopiter.service;

import autopiter.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import ru.skyb.autopiter.model.AuthData;

import java.util.*;

@Component
public class AutopiterService extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(AutopiterService.class);

    /*@Value("${spring.security.user.name}")
    private String name;

    @Value("${spring.security.user.password}")
    private String password;*/

    public AuthorizationResponse authorizationResponse(AuthData authData) {

        /*Authorization authorizationRequest = new Authorization();
        authorizationRequest.setUserID(userId);
        authorizationRequest.setPassword(password);
        authorizationRequest.setSave(Save);*/

        Authorization authorizationRequest = new Authorization();
        authorizationRequest.setUserID(authData.getLogin());
        authorizationRequest.setPassword(authData.getPassword());
        authorizationRequest.setSave(authData.isSave());
        setUathData(authData);

        return (AuthorizationResponse)getWebServiceTemplate().marshalSendAndReceive(authorizationRequest,
                new SoapActionCallback("http://www.autopiter.ru/Authorization"));
    }

    public AuthData setUathData(AuthData authData) {
        //authData.setLogin(authData.getLogin());
        return authData;
    }

    public boolean printAuthorizationResponse(AuthorizationResponse authorizationResponse) {
        boolean auth = authorizationResponse.isAuthorizationResult();
        log.info("Authorization {}", auth);
        return auth;
    }

    public GetPriceIdResponse getPriceIdResponse (Long ArticleId, int SearchCross)  {
        GetPriceId getPriceIdRequest = new GetPriceId();
        getPriceIdRequest.setArticleId(ArticleId);
        getPriceIdRequest.setSearchCross(SearchCross);
        //try {
            return (GetPriceIdResponse) getWebServiceTemplate().marshalSendAndReceive(getPriceIdRequest,
                    new SoapActionCallback("http://www.autopiter.ru/GetPriceId"));
        /*} catch (NullPointerException exception) {
            return null;
        }
        catch (Exception exception) {
            log.error("getPriceIdResponse{} failed: {}", exception.getMessage(), exception);
            throw new AutopiterServiceException("Превышено количество запросов к веб сервису. Вы блокированы на текущие сутки", exception);
        }*/
    }

    public List<PriceSearchModel> printGetPriceIdResponse(GetPriceIdResponse getPriceIdResponse) {
        return new ArrayList<>(getPriceIdResponse.getGetPriceIdResult().getPriceSearchModel());
    }

    public FindCatalogResponse findCatalogResponse (String Number)  {
        FindCatalog findCatalog = new FindCatalog();
        findCatalog.setNumber(Number);
        //try {
            return (FindCatalogResponse) getWebServiceTemplate().marshalSendAndReceive(findCatalog,
                    new SoapActionCallback("http://www.autopiter.ru/FindCatalog"));
        /*} catch (NullPointerException exception) {
            return null;
        }
        catch (Exception exception) {
            log.error("findCatalogResponse{} failed: {}", exception.getMessage(), exception);
            throw new AutopiterServiceException("Превышено количество запросов к веб сервису. Вы блокированы на текущие сутки", exception);
        }*/
    }

    public long printFindCatalogResponse(FindCatalogResponse findCatalog) {
        ArrayList<Integer> list = new ArrayList<>();
        for(SearchCatalogModel searchCatalogModel : findCatalog.getFindCatalogResult().getSearchCatalogModel()) {
            list.add(searchCatalogModel.getSalesRating());
        }
        for(SearchCatalogModel searchCatalogModel : findCatalog.getFindCatalogResult().getSearchCatalogModel()) {
            if (Collections.max(list) == searchCatalogModel.getSalesRating()) {
                return searchCatalogModel.getArticleId();
            }
        }
        return Long.valueOf("0");
    }



}
