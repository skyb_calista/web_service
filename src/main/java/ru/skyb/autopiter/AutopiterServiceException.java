package ru.skyb.autopiter;

public class AutopiterServiceException extends Exception {
    public AutopiterServiceException(String message, Exception exception) {
        super(message, exception);
    }
}
