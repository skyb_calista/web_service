package ru.skyb.autopiter;

import autopiter.wsdl.AuthorizationResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import ru.skyb.autopiter.config.AutopiterConfiguration;
import ru.skyb.autopiter.model.AuthData;
import ru.skyb.autopiter.service.AutopiterService;

@SpringBootApplication
@Configuration
public class AutopiterApplication {


    public static void main(String[] args) {
        //SpringApplication.run(AutopiterApplication.class, args);

        ApplicationContext ctx = SpringApplication.run(AutopiterApplication.class, args);

        AuthData authData = new AuthData();
        authData.setLogin("");
        authData.setPassword("");
        authData.setSave(false);

        AutopiterConfiguration autopiterConfiguration = ctx.getBean(AutopiterConfiguration.class);
        AutopiterService autopiterService  = autopiterConfiguration.autopiterService(autopiterConfiguration.marshaller(), authData);

        autopiterService.printAuthorizationResponse(autopiterService.authorizationResponse(authData));


        /*AutopiterService autopiterService = ctx.getBean(AutopiterService.class);
        AuthorizationResponse authorizationResponse = autopiterService.authorizationResponse("", "", true);
        autopiterService.printAuthorizationResponse(authorizationResponse);*/
    }
}
