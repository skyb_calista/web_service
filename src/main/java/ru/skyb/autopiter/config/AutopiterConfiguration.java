package ru.skyb.autopiter.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;
import ru.skyb.autopiter.model.AuthData;
import ru.skyb.autopiter.service.AutopiterService;

@Configuration
@Component
public class AutopiterConfiguration {

    private static final Logger log = LoggerFactory.getLogger(AutopiterConfiguration.class);

    /*@Value("${spring.security.user.name}")
    private String name;

    @Value("${spring.security.user.password}")
    private String password;*/

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("autopiter.wsdl");
        return marshaller;
    }


    @Bean
    public AutopiterService autopiterService(Jaxb2Marshaller marshaller, AuthData authData) {

        AutopiterService client = new AutopiterService();

        //log.info("NAME {} PASS {}", name, password);

        HttpComponentsMessageSender messageSender = new HttpComponentsMessageSender();
        //messageSender.setCredentials(new UsernamePasswordCredentials("411960", "7777777"));
        //messageSender.setCredentials(new UsernamePasswordCredentials(client.setUathData(), authData.getPassword()));


        client.setDefaultUri("http://service.autopiter.ru/v2/price");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        client.setMessageSender(messageSender);

        client.authorizationResponse(authData);

        return client;
    }
}
