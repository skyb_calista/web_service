package ru.skyb.autopiter.model;

public class SearchBean {
    //String name;
    private String number;
    public Float price;
    private Integer numberOfAvailable;
    private Integer numberOfDaysSupply;
    private String color;
    private String firma;


    public SearchBean(Integer numberOfAvailable, Integer numberOfDaysSupply, String number, Float price, String color, String firma) {

        this.number = number;
        this.price = price;
        this.numberOfAvailable = numberOfAvailable;
        this.numberOfDaysSupply = numberOfDaysSupply;
        this.color = color;
        this.firma = firma;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getNumberOfAvailable() {
        return numberOfAvailable;
    }

    public void setNumberOfAvailable(Integer numberOfAvailable) {
        this.numberOfAvailable = numberOfAvailable;
    }

    public Integer getNumberOfDaysSupply() {
        return numberOfDaysSupply;
    }

    public void setNumberOfDaysSupply(Integer numberOfDaysSupply) {
        this.numberOfDaysSupply = numberOfDaysSupply;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }
}
