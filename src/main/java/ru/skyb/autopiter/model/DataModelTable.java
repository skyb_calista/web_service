package ru.skyb.autopiter.model;

import java.io.Serializable;
import java.util.List;

public class DataModelTable implements Serializable {
    private List<FirstTable> firstTableList;

    private List<List<SearchBean>> searchBeanList;

    public List<FirstTable> getFirstTableList() {
        return firstTableList;
    }

    public void setFirstTableList(List<FirstTable> firstTableList) {
        this.firstTableList = firstTableList;
    }

    public List<List<SearchBean>> getSearchBeanList() {
        return searchBeanList;
    }

    public void setSearchBeanList(List<List<SearchBean>> searchBeanList) {
        this.searchBeanList = searchBeanList;
    }
}
