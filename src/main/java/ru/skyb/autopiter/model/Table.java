package ru.skyb.autopiter.model;

import java.util.List;

public class Table {
    private FirstTable firstTable;
    private List<SearchBean> searchBeanList;

    public Table(FirstTable firstTable, List<SearchBean> searchBeanList) {
        this.firstTable = firstTable;
        this.searchBeanList = searchBeanList;
    }

    public FirstTable getFirstTable() {
        return firstTable;
    }

    public void setFirstTable(FirstTable firstTable) {
        this.firstTable = firstTable;
    }

    public List<SearchBean> getSearchBeanList() {
        return searchBeanList;
    }

    public void setSearchBeanList(List<SearchBean> searchBeanList) {
        this.searchBeanList = searchBeanList;
    }
}
