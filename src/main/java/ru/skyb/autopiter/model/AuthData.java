package ru.skyb.autopiter.model;

import org.springframework.stereotype.Component;

@Component
public class AuthData {
    private String login;
    private String password;
    private boolean save;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }
}
