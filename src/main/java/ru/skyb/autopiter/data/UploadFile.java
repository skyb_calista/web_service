package ru.skyb.autopiter.data;

import autopiter.wsdl.FindCatalogResponse;
import autopiter.wsdl.GetPriceIdResponse;
import autopiter.wsdl.PriceSearchModel;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.soap.client.SoapFaultClientException;
import ru.skyb.autopiter.model.DataModelTable;
import ru.skyb.autopiter.model.FirstTable;
import ru.skyb.autopiter.model.SearchBean;
import ru.skyb.autopiter.service.AutopiterService;

import java.io.*;

import java.util.*;
import java.util.stream.Collectors;

@Controller
public class UploadFile {

    public final AutopiterService autopiterService;
    private static final Logger log = LoggerFactory.getLogger(UploadFile.class);

    @Autowired
    public UploadFile(AutopiterService autopiterService) {
        this.autopiterService = autopiterService;
    }

    @PostMapping("/uploadExcelFile")
    public String uploadFile(Model model, MultipartFile file) throws IOException, InterruptedException {
        InputStream ExcelFileToRead = file.getInputStream();
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook(ExcelFileToRead);
        XSSFSheet sheet = xssfWorkbook.getSheetAt(0);
        Iterator rows = sheet.rowIterator();

        List<FirstTable> firstTableList = new ArrayList<>();

        DataModelTable dataModelTable = new DataModelTable();
        List<List<SearchBean>> searchBeanList = new ArrayList<>();

        //List<Table> tableList = new ArrayList<>();

        int count = 0;
        //try {
        while (rows.hasNext()) {
            try {
                XSSFRow row = (XSSFRow) rows.next();
                //if (row.getCell(1) != null && row.getCell(1).getCellType() == CellType.STRING) {
                if (row.getCell(1) != null) {
                    FirstTable firstTable = new FirstTable();

                    XSSFCell cellNumber = row.getCell(1);
                    String stringCellNumber;
                    if(row.getCell(1).getCellType() == CellType.NUMERIC) {
                        stringCellNumber = String.valueOf(cellNumber.getNumericCellValue()).replaceAll("\\.","");
                        stringCellNumber = stringCellNumber.substring(0, stringCellNumber.length() - 3);
                    }
                    else {
                        stringCellNumber = cellNumber.getStringCellValue();
                    }


                    FindCatalogResponse findCatalogResponse = autopiterService.findCatalogResponse(stringCellNumber);
                    GetPriceIdResponse getPriceIdResponse = autopiterService.getPriceIdResponse(autopiterService.printFindCatalogResponse(findCatalogResponse), 0);

                    List<PriceSearchModel> priceSearchModelList = autopiterService.printGetPriceIdResponse(getPriceIdResponse);

                    if(autopiterService.printGetPriceIdResponse(getPriceIdResponse) != null) {



                        XSSFCell cellName = row.getCell(2);
                        if (cellName != null) {
                            if (cellName.getCellType() == CellType.STRING) {
                                firstTable.setName(cellName.getStringCellValue());
                            }
                        }
                        XSSFCell cellPrice = row.getCell(3);
                        if (cellPrice != null) {
                            firstTable.setPrice(cellPrice.getNumericCellValue());
                        }
                        XSSFCell cellFirm = row.getCell(0);
                        if (cellFirm != null) {
                            firstTable.setFirm(cellFirm.getStringCellValue());
                        }


                        firstTable.setNumber(stringCellNumber);

                        Set<SearchBean> set = new TreeSet<>(Comparator.comparing(searchbean -> searchbean.price));

                        for (PriceSearchModel priceSearchModel : priceSearchModelList) {

                            double price = (double) 0;

                            if (row.getCell(3) != null) {
                                price = row.getCell(3).getNumericCellValue();
                            }
                            log.info("CurrencyName {} CatalogName {} Region {} ShotNumber {}, SellerId {}", priceSearchModel.getCurrencyName(), priceSearchModel.getCatalogName(), priceSearchModel.getRegion(), priceSearchModel.getShotNumber(), priceSearchModel.getSellerId());
                            //CurrencyName RUB CatalogName Hyundai-Kia Region Москва ShotNumber 252812b010
                            //log.info("SellerId {}",priceSearchModel.getSellerId());

                            //GENERAL MOTORS	12631195	Пистон 12631195	1 650,00	3,000 позиция 36

                            String firma = String.valueOf(priceSearchModel.getSellerId());
                            set.addAll(Collections.singletonList(new SearchBean(priceSearchModel.getNumberOfAvailable(), priceSearchModel.getNumberOfDaysSupply(), priceSearchModel.getNumber(), priceSearchModel.getSalePrice(), getColor(price, priceSearchModel.getSalePrice()), firma)));
                            if (priceSearchModelList.size() == 1) {
                                set.addAll(Arrays.asList(new SearchBean(-1, 0, "0", (float) -1, "white", firma),
                                        new SearchBean(0, 0, "0", (float) 0, "white", firma)));
                                //log.info("TEST ");
                                //set.addAll(Collections.singletonList(new SearchBean(0, 0, "0", (float) 0, "white", firma)));
                            }
                            if (priceSearchModelList.size() == 2) {
                                set.addAll(Collections.singletonList(new SearchBean(0, 0, "0", (float) 0, "white", firma)));
                            }

                        }
                        log.info("ПОЗИЦИЯ {} cellNumber {} size {}", count, stringCellNumber, priceSearchModelList.size());


                        searchBeanList.add(set.stream().limit(3).collect(Collectors.toList()));

                        firstTableList.add(firstTable);


                        //tableList.add(new Table(firstTable, set.stream().limit(3).collect(Collectors.toList())));

                        //log.info("cellNumber {}", stringCellNumber);
                        //}

                        //log.info("ПОЗИЦИЯ {}", count);
                    }
                    /*if (count == 3) {
                        dataModelTable.setSearchBeanList(searchBeanList);
                        dataModelTable.setFirstTableList(firstTableList);

                        model.addAttribute("data", dataModelTable);
                        return "comparisonTable";
                    }*/
                }
            }catch(NullPointerException exception){
                //Thread.sleep(150000);
                //exception.printStackTrace();
                /*dataModelTable.setSearchBeanList(searchBeanList);
                dataModelTable.setFirstTableList(firstTableList);

                model.addAttribute("data", dataModelTable);
                model.addAttribute("error", exception.getCause());*/
                //log.error("exception {} line {}", exception.getLocalizedMessage(), exception.getStackTrace());
                //log.error("Failed: {}", exception.getLocalizedMessage(), exception);
                //return "comparisonTable";
            }
            //catch(AutopiterServiceException exception){
            catch(SoapFaultClientException exception){
                try {
                    dataModelTable.setSearchBeanList(searchBeanList);
                    dataModelTable.setFirstTableList(firstTableList);

                    model.addAttribute("data", dataModelTable);
                    //model.addAttribute("data", tableList);
                    model.addAttribute("error", exception.getMessage());
                    //log.error(exception.getLocalizedMessage());
                    log.error("Failed Soap exception: {}", exception.getLocalizedMessage(), exception);
                } catch (Exception e) {
                    log.error("MegaException", e);
                }
                return "comparisonTable";
            }
            count++;

        }
        /*for(Table table : tableList){
            log.info(table.getFirstTable().number);
        }*/
        /*} catch (SoapFaultClientException exception) {

            dataModelTable.setSearchBeanList(searchBeanList);
            dataModelTable.setFirstTableList(firstTableList);

            model.addAttribute("data", dataModelTable);
            model.addAttribute("error", exception.getFaultStringOrReason());
            log.error(exception.getLocalizedMessage());
            return "comparisonTable";
        }*/

        //table.add(firstTableList)

        dataModelTable.setSearchBeanList(searchBeanList);
        dataModelTable.setFirstTableList(firstTableList);

        model.addAttribute("data", dataModelTable);

        return "comparisonTable";
    }


    private String getColor(double price, float salePrice) {
        if(price > salePrice) {
            return "red";
        }
        return "green";
    }
}
