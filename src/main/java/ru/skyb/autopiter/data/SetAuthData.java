package ru.skyb.autopiter.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.skyb.autopiter.config.AutopiterConfiguration;
import ru.skyb.autopiter.model.AuthData;
import ru.skyb.autopiter.service.AutopiterService;

import java.io.IOException;

@Controller
public class SetAuthData {
    private static final Logger log = LoggerFactory.getLogger(UploadFile.class);

    @Autowired
    private ApplicationContext appContext;

    @PostMapping("/setAuthData")
    public String setAuthData(Model model, @ModelAttribute AuthData authData) throws IOException, InterruptedException {



        AutopiterConfiguration autopiterConfiguration = appContext.getBean(AutopiterConfiguration.class);
        AutopiterService autopiterService  = autopiterConfiguration.autopiterService(autopiterConfiguration.marshaller(), authData);

        boolean auth = autopiterService.printAuthorizationResponse(autopiterService.authorizationResponse(authData));
        //log.info("Authorization {}", auth);
        model.addAttribute("login", authData.getLogin());
        if(auth) {
            model.addAttribute("auth", "Успешно");
        }
        else {
            model.addAttribute("auth", "Ошибка авторизации");
        }

        //return "redirect:/";
        return  "index";
    }
}
